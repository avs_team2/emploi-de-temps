-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:3306
-- Généré le : mer. 06 jan. 2021 à 21:49
-- Version du serveur :  8.0.22-0ubuntu0.20.04.3
-- Version de PHP : 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `emploi_tempsdb`
--

-- --------------------------------------------------------

--
-- Structure de la table `Departements`
--

CREATE TABLE `Departements` (
  `idDepartement` int NOT NULL,
  `codeDepartement` varchar(255) NOT NULL,
  `nameDepartement` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `Departements`
--

INSERT INTO `Departements` (`idDepartement`, `codeDepartement`, `nameDepartement`) VALUES
(1, 'infotel', 'informatique'),
(2, 'architecture', 'genie civil et architecture'),
(3, 'hymaie', 'hydraulique des eaux'),
(4, 'scievie', 'science en vie'),
(5, 'agepd', 'agriculture et elevage');

-- --------------------------------------------------------

--
-- Structure de la table `Enseignants`
--

CREATE TABLE `Enseignants` (
  `idEnseignant` int NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `titre` varchar(255) NOT NULL,
  `mail` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `NiveauOption`
--

CREATE TABLE `NiveauOption` (
  `idNiveau` int NOT NULL,
  `idOption` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `Niveaus`
--

CREATE TABLE `Niveaus` (
  `idNiveau` int NOT NULL,
  `nameLevel` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `Options`
--

CREATE TABLE `Options` (
  `idOption` int NOT NULL,
  `nameOption` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `Periodes`
--

CREATE TABLE `Periodes` (
  `idPeriode` int NOT NULL,
  `namePeriode` varchar(255) NOT NULL,
  `dateDebutPeriode` varchar(255) NOT NULL,
  `dateFinPeriode` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `Periodes`
--

INSERT INTO `Periodes` (`idPeriode`, `namePeriode`, `dateDebutPeriode`, `dateFinPeriode`) VALUES
(1, 'periode  du lundi', '10-12-2010', '10-12-2010');

-- --------------------------------------------------------

--
-- Structure de la table `Salles`
--

CREATE TABLE `Salles` (
  `idSalle` int NOT NULL,
  `nameSalle` varchar(255) NOT NULL,
  `nombreSalle` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `Seance`
--

CREATE TABLE `Seance` (
  `idUnite` int NOT NULL,
  `idSalle` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `Semestres`
--

CREATE TABLE `Semestres` (
  `idSemestre` int NOT NULL,
  `nameSemestre` varchar(255) NOT NULL,
  `nombreSemestre` varchar(255) NOT NULL,
  `dateDebutSemestre` varchar(255) NOT NULL,
  `dateFinSemestre` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `UniteEnseignant`
--

CREATE TABLE `UniteEnseignant` (
  `idUnite` int NOT NULL,
  `idEnseignant` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `UnitePeriode`
--

CREATE TABLE `UnitePeriode` (
  `idUnite` int NOT NULL,
  `idPeriode` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `Unites`
--

CREATE TABLE `Unites` (
  `idUnite` int NOT NULL,
  `codeUnite` varchar(255) NOT NULL,
  `nameUnite` varchar(255) NOT NULL,
  `nombrereditUnite` varchar(255) NOT NULL,
  `DepartementID` int NOT NULL,
  `SemestreID` int NOT NULL,
  `NiveauID` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `Users`
--

CREATE TABLE `Users` (
  `idUser` int NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `mail` varchar(45) DEFAULT NULL,
  `DepartementID` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `Users`
--

INSERT INTO `Users` (`idUser`, `username`, `password`, `mail`, `DepartementID`) VALUES
(1, 'ngounou', 'ngounou', 'youssoufngounou@gmail.com', 1);

-- --------------------------------------------------------

--
-- Structure de la table `__EFMigrationsHistory`
--

CREATE TABLE `__EFMigrationsHistory` (
  `MigrationId` varchar(95) NOT NULL,
  `ProductVersion` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `__EFMigrationsHistory`
--

INSERT INTO `__EFMigrationsHistory` (`MigrationId`, `ProductVersion`) VALUES
('20201210200207_InitialCreate', '3.1.8');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `Departements`
--
ALTER TABLE `Departements`
  ADD PRIMARY KEY (`idDepartement`);

--
-- Index pour la table `Enseignants`
--
ALTER TABLE `Enseignants`
  ADD PRIMARY KEY (`idEnseignant`);

--
-- Index pour la table `NiveauOption`
--
ALTER TABLE `NiveauOption`
  ADD PRIMARY KEY (`idNiveau`,`idOption`),
  ADD KEY `IX_NiveauOption_idOption` (`idOption`);

--
-- Index pour la table `Niveaus`
--
ALTER TABLE `Niveaus`
  ADD PRIMARY KEY (`idNiveau`);

--
-- Index pour la table `Options`
--
ALTER TABLE `Options`
  ADD PRIMARY KEY (`idOption`);

--
-- Index pour la table `Periodes`
--
ALTER TABLE `Periodes`
  ADD PRIMARY KEY (`idPeriode`);

--
-- Index pour la table `Salles`
--
ALTER TABLE `Salles`
  ADD PRIMARY KEY (`idSalle`);

--
-- Index pour la table `Seance`
--
ALTER TABLE `Seance`
  ADD PRIMARY KEY (`idUnite`,`idSalle`),
  ADD KEY `IX_Seance_idSalle` (`idSalle`);

--
-- Index pour la table `Semestres`
--
ALTER TABLE `Semestres`
  ADD PRIMARY KEY (`idSemestre`);

--
-- Index pour la table `UniteEnseignant`
--
ALTER TABLE `UniteEnseignant`
  ADD PRIMARY KEY (`idUnite`,`idEnseignant`),
  ADD KEY `IX_UniteEnseignant_idEnseignant` (`idEnseignant`);

--
-- Index pour la table `UnitePeriode`
--
ALTER TABLE `UnitePeriode`
  ADD PRIMARY KEY (`idUnite`,`idPeriode`),
  ADD KEY `IX_UnitePeriode_idPeriode` (`idPeriode`);

--
-- Index pour la table `Unites`
--
ALTER TABLE `Unites`
  ADD PRIMARY KEY (`idUnite`),
  ADD KEY `IX_Unites_DepartementID` (`DepartementID`),
  ADD KEY `IX_Unites_NiveauID` (`NiveauID`),
  ADD KEY `IX_Unites_SemestreID` (`SemestreID`);

--
-- Index pour la table `Users`
--
ALTER TABLE `Users`
  ADD PRIMARY KEY (`idUser`),
  ADD KEY `IX_Users_DepartementID` (`DepartementID`);

--
-- Index pour la table `__EFMigrationsHistory`
--
ALTER TABLE `__EFMigrationsHistory`
  ADD PRIMARY KEY (`MigrationId`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `Departements`
--
ALTER TABLE `Departements`
  MODIFY `idDepartement` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT pour la table `Enseignants`
--
ALTER TABLE `Enseignants`
  MODIFY `idEnseignant` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `Niveaus`
--
ALTER TABLE `Niveaus`
  MODIFY `idNiveau` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `Options`
--
ALTER TABLE `Options`
  MODIFY `idOption` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `Periodes`
--
ALTER TABLE `Periodes`
  MODIFY `idPeriode` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `Salles`
--
ALTER TABLE `Salles`
  MODIFY `idSalle` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `Semestres`
--
ALTER TABLE `Semestres`
  MODIFY `idSemestre` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `Unites`
--
ALTER TABLE `Unites`
  MODIFY `idUnite` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `Users`
--
ALTER TABLE `Users`
  MODIFY `idUser` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `NiveauOption`
--
ALTER TABLE `NiveauOption`
  ADD CONSTRAINT `FK_NiveauOption_Niveaus_idNiveau` FOREIGN KEY (`idNiveau`) REFERENCES `Niveaus` (`idNiveau`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_NiveauOption_Options_idOption` FOREIGN KEY (`idOption`) REFERENCES `Options` (`idOption`) ON DELETE CASCADE;

--
-- Contraintes pour la table `Seance`
--
ALTER TABLE `Seance`
  ADD CONSTRAINT `FK_Seance_Salles_idSalle` FOREIGN KEY (`idSalle`) REFERENCES `Salles` (`idSalle`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_Seance_Unites_idUnite` FOREIGN KEY (`idUnite`) REFERENCES `Unites` (`idUnite`) ON DELETE CASCADE;

--
-- Contraintes pour la table `UniteEnseignant`
--
ALTER TABLE `UniteEnseignant`
  ADD CONSTRAINT `FK_UniteEnseignant_Enseignants_idEnseignant` FOREIGN KEY (`idEnseignant`) REFERENCES `Enseignants` (`idEnseignant`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_UniteEnseignant_Unites_idUnite` FOREIGN KEY (`idUnite`) REFERENCES `Unites` (`idUnite`) ON DELETE CASCADE;

--
-- Contraintes pour la table `UnitePeriode`
--
ALTER TABLE `UnitePeriode`
  ADD CONSTRAINT `FK_UnitePeriode_Periodes_idPeriode` FOREIGN KEY (`idPeriode`) REFERENCES `Periodes` (`idPeriode`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_UnitePeriode_Unites_idUnite` FOREIGN KEY (`idUnite`) REFERENCES `Unites` (`idUnite`) ON DELETE CASCADE;

--
-- Contraintes pour la table `Unites`
--
ALTER TABLE `Unites`
  ADD CONSTRAINT `FK_Unites_Departements_DepartementID` FOREIGN KEY (`DepartementID`) REFERENCES `Departements` (`idDepartement`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_Unites_Niveaus_NiveauID` FOREIGN KEY (`NiveauID`) REFERENCES `Niveaus` (`idNiveau`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_Unites_Semestres_SemestreID` FOREIGN KEY (`SemestreID`) REFERENCES `Semestres` (`idSemestre`) ON DELETE CASCADE;

--
-- Contraintes pour la table `Users`
--
ALTER TABLE `Users`
  ADD CONSTRAINT `FK_Users_Departements_DepartementID` FOREIGN KEY (`DepartementID`) REFERENCES `Departements` (`idDepartement`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
